package com.soprasteria.smartup;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.soprasteria.smartup.dto.ActorDto;
import com.soprasteria.smartup.dto.MapperActorDtoEntity;
import com.soprasteria.smartup.dto.MapperMovieDtoEntity;
import com.soprasteria.smartup.dto.MovieDto;
import com.soprasteria.smartup.dto.MovieWithTitleAndYearDto;
import com.soprasteria.smartup.interfaces.operationsActor;
import com.soprasteria.smartup.interfaces.operationsMovie;
import com.soprasteria.smartup.repositories.ActRepository;
import com.soprasteria.smartup.repositories.MovieRepository;
import com.soprasteria.smartup.servicies.ActorService;
import com.soprasteria.smartup.servicies.MovieService;
import com.soprasteria.smartup.models.*;

@ExtendWith(MockitoExtension.class)
class testMovie{

	@Mock
	private MovieRepository movieRepository;
	
	@Mock
	private ActRepository actRepository;
	
	
	@InjectMocks
	private operationsMovie myServiceMovies=new MovieService(movieRepository);
	
	@InjectMocks
	private operationsActor myServiceActor=new ActorService(movieRepository,actRepository);

	@Test
	void shouldReturngetAllMovies() {
		//given
		List<Movie> movieList=new ArrayList<>();
		Movie movieMock=new Movie();
		movieMock.setGenre("El mejor");
		movieMock.setReleaseDate("Ayer");
		movieMock.setTitle("Sharknado");
		movieList.add(movieMock);
		
		Collection<MovieDto> conversor=new ArrayList<>();
		when(movieRepository.findAll()).thenReturn(movieList);
	
		//when
		Collection<MovieDto> lista=myServiceMovies.getAll();
		
		movieList.stream().forEach(m-> conversor.add(MapperMovieDtoEntity.entityToDto(m)));
		
		//then
		assertThat(conversor).isEqualTo(lista);
	}

	@Test
	void shouldReturnMoviesWithTitleAndYear() {
		//given
		List<Movie> movieList=new ArrayList<>();
		Movie movieMock=new Movie();
		movieMock.setGenre("El mejor");
		movieMock.setReleaseDate("Ayer");
		movieMock.setTitle("Sharknado");
		movieList.add(movieMock);
		
		Collection<MovieWithTitleAndYearDto> conversor=new ArrayList<>();
		Collection<MovieWithTitleAndYearDto> movieDtoToSimpleMovie= new ArrayList<>();
		when(movieRepository.findAll()).thenReturn(movieList);
		//when
		
		Collection<MovieWithTitleAndYearDto> lista=myServiceMovies.getMoviesWithTitleAndYear();
		
		movieList.stream().forEach(m-> movieDtoToSimpleMovie.add(new MovieWithTitleAndYearDto(MapperMovieDtoEntity.entityToDto(m))));
		lista.stream().forEach(m-> conversor.add(m));

		//then
		assertThat(conversor).isEqualTo(movieDtoToSimpleMovie);
	}

	@Test
	void shouldReturnDetailsMovie() {
		List<Movie> movieList=new ArrayList<>();
		Movie movieMock=new Movie();
		movieMock.setGenre("El mejor");
		movieMock.setReleaseDate("Ayer");
		movieMock.setTitle("Sharknado");
		movieList.add(movieMock);
		
		Collection<MovieDto> conversor=new ArrayList<>();
		
		when(movieRepository.findByTitle("Sharknado")).thenReturn(movieList);

		//when
		Collection<MovieDto> lista=myServiceMovies.getDetailsMovie("Sharknado");
		
		movieList.stream().forEach(m->conversor.add(MapperMovieDtoEntity.entityToDto(m)));
		//then
		assertThat(conversor).isEqualTo(lista);
	}

	@Test
	void shouldInsertMovie() {
		//given
		MovieDto peliResultado= new MovieDto();
		peliResultado.setGenre("accion");
		peliResultado.setRelease_date("proximamente");
		peliResultado.setTitle("Spoderman");
		
		List<Movie> movieList=new ArrayList<>();
		Movie movieMock=new Movie();
		movieMock.setGenre("El mejor");
		movieMock.setReleaseDate("Ayer");
		movieMock.setTitle("Sharknado");
		movieList.add(movieMock);
		
		Movie peliParaElRepositorio=new Movie();
		peliParaElRepositorio.setGenre("accion");
		peliParaElRepositorio.setReleaseDate("proximamente");
		peliParaElRepositorio.setTitle("Spoderman");
		
		MovieDto peliParaElRepositorioDto=new MovieDto();
		peliParaElRepositorioDto.setGenre("accion");
		peliParaElRepositorioDto.setRelease_date("proximamente");
		peliParaElRepositorioDto.setTitle("Spoderman");
		

		when(movieRepository.save(peliParaElRepositorio)).thenReturn(peliParaElRepositorio);		
		//when
		myServiceMovies.insertMovie(peliParaElRepositorioDto);
		
		//then
		assertThat(peliParaElRepositorioDto).isEqualTo(peliResultado);

	}
	
	@Test
	void shouldRemoveMovie() {
		//given
		List<Movie> movieList=new ArrayList<>();
		Movie movieMock=new Movie();
		movieMock.setGenre("El mejor");
		movieMock.setReleaseDate("Ayer");
		movieMock.setTitle("Sharknado");
		movieList.add(movieMock);
		
//		when(movieRepository.delete(movieMock));
	}
	//revisar
	@Test
	void shouldReturnActors() {
		
		//given
		List<Movie> movieList=new ArrayList<>();
		Movie movieMock=new Movie();
		movieMock.setGenre("El mejor");
		movieMock.setReleaseDate("Ayer");
		movieMock.setTitle("Sharknado");
		movieList.add(movieMock);
		
		List<Act> actList  =new ArrayList<>();
		Actor actorMock=new Actor();
		actorMock.setName("Juanma");
		actorMock.setSurname("Moreno");
		actorMock.setNumberOscars(2);
		
		Act act1= new Act();
		act1.setActor(actorMock);
		act1.setMovy(movieMock);
		act1.setId(3);
		actList.add(act1);
		List<Actor> actorList=new ArrayList<>();
		actorList.add(actorMock);
		
		Collection<Movie> listaPelis=new ArrayList<>();
		Collection<Act> actsEncontradas= new ArrayList<>();
		Collection<ActorDto> actoresEncontrados= new ArrayList<>();
		Collection<ActorDto> actorListDto=new ArrayList<>();
		
		when(movieRepository.findByTitle("Sharknado")).thenReturn(movieList);
		
		when(actRepository.findByMovie(0)).thenReturn(actList);
		//when
		listaPelis=movieRepository.findByTitle("Sharknado");
		actsEncontradas=actRepository.findByMovie(1);
			
		actsEncontradas.stream().map(m->actRepository.findByMovie(m.getId())).collect(Collectors.toList());
		for(Movie mov:listaPelis) {
			actsEncontradas.addAll(actRepository.findByMovie(mov.getId()));
		}
		
		for(Act a:actsEncontradas) {
			actoresEncontrados.add(MapperActorDtoEntity.entityToDto(a.getActor()));
		}
		
		for(Actor actor:actorList) {
			actorListDto.add(MapperActorDtoEntity.entityToDto(actor));
		}
		//then
		assertThat(actoresEncontrados).isEqualTo(actorListDto);
	}

}
