package com.soprasteria.smartup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AmazingPraimVideoApplication {

	public static void main(String[] args) {
		SpringApplication.run(AmazingPraimVideoApplication.class, args);
	}

}
