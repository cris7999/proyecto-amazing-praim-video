package com.soprasteria.smartup.interfaces;

import java.util.Collection;

import com.soprasteria.smartup.dto.*;

public interface operationsMovie {
	public Collection<MovieDto> getAll();
	public Collection<MovieWithTitleAndYearDto> getMoviesWithTitleAndYear();
	public Collection<MovieDto> getDetailsMovie(String name);
	public void insertMovie(MovieDto mdto);
	public void deleteMovie(int id);
	
}
