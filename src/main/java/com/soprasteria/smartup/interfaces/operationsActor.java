package com.soprasteria.smartup.interfaces;

import java.util.Collection;

import com.soprasteria.smartup.dto.ActorDto;

public interface operationsActor {
	public Collection<ActorDto> getActors(String name);
}
