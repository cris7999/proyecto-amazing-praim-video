package com.soprasteria.smartup.servicies;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.soprasteria.smartup.controllers.MovieController;
import com.soprasteria.smartup.dto.ActorDto;
import com.soprasteria.smartup.dto.MapperActorDtoEntity;
import com.soprasteria.smartup.interfaces.operationsActor;
import com.soprasteria.smartup.models.Act;
import com.soprasteria.smartup.models.Movie;
import com.soprasteria.smartup.repositories.ActRepository;
import com.soprasteria.smartup.repositories.MovieRepository;

@Service
public class ActorService implements operationsActor{

	private static final Logger logger = LogManager.getLogger(MovieController.class);
	
	private MovieRepository movieRepository;
	
	private ActRepository actRepository;
	
	@Autowired
	public ActorService(MovieRepository mr,ActRepository ar) {
		movieRepository=mr;
		actRepository=ar;
	}
	
	@Override
	public Collection<ActorDto> getActors(String movieName) {
		logger.info("Trying get all actors from a movie");
		
		Collection<Movie> movies;
		Collection<ActorDto> actors=new ArrayList<>();
		Collection<Act> acts = new ArrayList<>();
		
		movies=movieRepository.findByTitle(movieName);
		
		for(Movie mov:movies) {
			acts.addAll(actRepository.findByMovie(mov.getId()));
		}
		
		for(Act a:acts) {
			actors.add(MapperActorDtoEntity.entityToDto(a.getActor()));
		}
		
		return actors;
	}
}