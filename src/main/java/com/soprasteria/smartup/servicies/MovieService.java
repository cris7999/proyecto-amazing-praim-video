package com.soprasteria.smartup.servicies;

import java.util.Collection;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.soprasteria.smartup.controllers.MovieController;
import com.soprasteria.smartup.dto.MapperMovieDtoEntity;
import com.soprasteria.smartup.dto.MovieDto;
import com.soprasteria.smartup.dto.MovieWithTitleAndYearDto;
import com.soprasteria.smartup.exceptions.MovieNotDeleted;
import com.soprasteria.smartup.exceptions.MovieNotInserted;
import com.soprasteria.smartup.interfaces.operationsMovie;

import com.soprasteria.smartup.repositories.MovieRepository;

@Service
public class MovieService implements operationsMovie {

	private static final Logger logger = LogManager.getLogger(MovieController.class);

	private MovieRepository movieRepository;
	
	@Autowired
	public MovieService(MovieRepository movieRepository) {
		this.movieRepository=movieRepository;
	}

	@Override
	public Collection<MovieDto> getAll() {
		logger.info("Trying get all Movies");
		return movieRepository.findAll().stream().map(MapperMovieDtoEntity::entityToDto).collect(Collectors.toList());		
	}

	@Override
	public Collection<MovieWithTitleAndYearDto> getMoviesWithTitleAndYear() {
		logger.info("Trying get movies with with title and year");
		return movieRepository.findAll().stream().map( m -> new MovieWithTitleAndYearDto(MapperMovieDtoEntity.entityToDto(m)))
											 .collect(Collectors.toList());
	}

	@Override
	public Collection<MovieDto> getDetailsMovie(String title) {
		logger.info("Trying get details of a movie");
		return movieRepository.findByTitle(title).stream().map(MapperMovieDtoEntity::entityToDto).collect(Collectors.toList());
	}
	
	@Override
	public void insertMovie(MovieDto mdto) {
		logger.info("Trying to insert a movie");
		
		if(movieRepository.findByTitle(mdto.getTitle()).isEmpty()) {
			try {
				movieRepository.save(MapperMovieDtoEntity.dtoToEntity(mdto));
				logger.info("Movie inserted");
			}catch(RuntimeException e) {
				logger.error("Error trying to persist the Movie");
			}
		}else
			throw new MovieNotInserted(mdto.getTitle());
	}
	@Override
	public void deleteMovie(int id) {
		movieRepository.delete(movieRepository.findById(id).orElseThrow(()->new MovieNotDeleted(id)));		
	}
}
