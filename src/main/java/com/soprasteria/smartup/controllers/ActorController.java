package com.soprasteria.smartup.controllers;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.soprasteria.smartup.dto.ActorDto;
import com.soprasteria.smartup.exceptions.ActorNotFound;
import com.soprasteria.smartup.interfaces.operationsActor;

@RestController
@RequestMapping("actor")
public class ActorController {
	 
	
	public operationsActor operations;
	
	@Autowired
	public ActorController(operationsActor operations) {
		this.operations=operations;
	}
	
	@GetMapping("/getActors/{name}")
	public Collection<ActorDto> getActors(@PathVariable String name) throws ActorNotFound{
		Collection<ActorDto> actorList= new ArrayList<>();
		actorList=operations.getActors(name);
		if( actorList.isEmpty() ) {
			throw new ActorNotFound();
		}else
			return operations.getActors(name);
	}
}