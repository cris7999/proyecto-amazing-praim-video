package com.soprasteria.smartup.controllers;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.soprasteria.smartup.dto.*;
import com.soprasteria.smartup.exceptions.MovieNotFound;
import com.soprasteria.smartup.interfaces.operationsMovie;

@RestController
@RequestMapping("movie")
public class MovieController {
//urls , movie/spiderman/actores

	private operationsMovie operations;
	
	@Autowired
	public MovieController(operationsMovie operations) {
		this.operations=operations;
	}
	
	@GetMapping("")
	public Collection<MovieDto> getMovies(){
		
		return operations.getAll();
	}
	@GetMapping("/getAllTitlesAndYear")
	public Collection<MovieWithTitleAndYearDto> getMoviesWithTitleAndYear(){
		
		return operations.getMoviesWithTitleAndYear();
	}
	
	@GetMapping("/{title}")
	public Collection<MovieDto> getDetailsMovie(@PathVariable String title) {
		return operations.getDetailsMovie(title);
	}
	
	@ResponseStatus(value = HttpStatus.CREATED)
	@PostMapping("/insert")
	public void insertMovie(MovieDto mdto) throws MovieNotFound {
		operations.insertMovie(mdto);		
	}
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	@DeleteMapping("/delete/{id}")
	public void delete(@PathVariable int id) {
		operations.deleteMovie(id);
	}
}