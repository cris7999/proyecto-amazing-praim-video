package com.soprasteria.smartup.models;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the MOVIES database table.
 * 
 */
@Entity
@Table(name="MOVIES")
public class Movie implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="MOVIE_GENERATOR",initialValue = 1,allocationSize = 1, sequenceName="SEC_MOVIES")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="MOVIE_GENERATOR")
	private int id;

	private String genre;

	@Column(name="RELEASE_DATE")
	private String releaseDate;

	private String title;

	//bi-directional many-to-one association to Act
	@OneToMany(mappedBy="movie")
	private List<Act> acts;

	public Movie() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getGenre() {
		return this.genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public String getReleaseDate() {
		return this.releaseDate;
	}

	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<Act> getActs() {
		return this.acts;
	}

	public void setActs(List<Act> acts) {
		this.acts = acts;
	}

	public Act addAct(Act act) {
		getActs().add(act);
		act.setMovy(this);

		return act;
	}

	public Act removeAct(Act act) {
		getActs().remove(act);
		act.setMovy(null);

		return act;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Movie other = (Movie) obj;
		if (acts == null) {
			if (other.acts != null)
				return false;
		} else if (!acts.equals(other.acts))
			return false;
		if (genre == null) {
			if (other.genre != null)
				return false;
		} else if (!genre.equals(other.genre))
			return false;
		if (releaseDate == null) {
			if (other.releaseDate != null)
				return false;
		} else if (!releaseDate.equals(other.releaseDate))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

}