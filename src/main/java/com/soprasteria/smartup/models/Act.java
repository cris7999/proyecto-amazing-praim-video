package com.soprasteria.smartup.models;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the ACT database table.
 * 
 */
@Entity
@NamedQuery(name="Act.findAll", query="SELECT a FROM Act a")
public class Act implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	//bi-directional many-to-one association to Actor
	@ManyToOne
	@JoinColumn(name="ID_ACTOR")
	private Actor actor;

	//bi-directional many-to-one association to Movie
	@ManyToOne
	@JoinColumn(name="ID_MOVIE")
	private Movie movie;

	public Act() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Actor getActor() {
		return this.actor;
	}

	public void setActor(Actor actor) {
		this.actor = actor;
	}

	public Movie getMovie() {
		return this.movie;
	}

	public void setMovy(Movie movie) {
		this.movie = movie;
	}

}