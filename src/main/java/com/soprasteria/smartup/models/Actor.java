package com.soprasteria.smartup.models;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="ACTORS")
public class Actor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	private String name;

	//probar sin @column
	@Column(name="NUMBER_OSCARS")
	private int numberOscars;

	private String surname;

	//bi-directional many-to-one association to Act
	@OneToMany(mappedBy="actor")
	private List<Act> acts;


	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNumberOscars() {
		return this.numberOscars;
	}

	public void setNumberOscars(int numberOscars) {
		this.numberOscars = numberOscars;
	}

	public String getSurname() {
		return this.surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public List<Act> getActs() {
		return this.acts;
	}

	public void setActs(List<Act> acts) {
		this.acts = acts;
	}
	//elimunar add y remove
	public Act addAct(Act act) {
		getActs().add(act);
		act.setActor(this);

		return act;
	}

	public Act removeAct(Act act) {
		getActs().remove(act);
		act.setActor(null);

		return act;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Actor other = (Actor) obj;
		if (acts == null) {
			if (other.acts != null)
				return false;
		} else if (!acts.equals(other.acts))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (numberOscars != other.numberOscars)
			return false;
		if (surname == null) {
			if (other.surname != null)
				return false;
		} else if (!surname.equals(other.surname))
			return false;
		return true;
	}

}