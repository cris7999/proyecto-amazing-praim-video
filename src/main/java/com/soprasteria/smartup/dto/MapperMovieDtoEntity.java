package com.soprasteria.smartup.dto;

import com.soprasteria.smartup.models.Movie;

public class MapperMovieDtoEntity {
	
	public static MovieDto entityToDto(Movie me) {
		MovieDto mdto=new MovieDto();
		mdto.setGenre(me.getGenre());
		mdto.setRelease_date(me.getReleaseDate());
		mdto.setTitle(me.getTitle());
		return mdto;
	}
	public static Movie dtoToEntity(MovieDto mdto) {
		Movie em=new Movie();
		em.setGenre(mdto.getGenre());
		em.setReleaseDate(mdto.getRelease_date());
		em.setTitle(mdto.getTitle());
		return em;
	}
}
