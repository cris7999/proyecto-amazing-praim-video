package com.soprasteria.smartup.dto;

public class MovieWithTitleAndYearDto {
	private String title;
	private String release_date;
	
	public MovieWithTitleAndYearDto(MovieDto mdto) {
		this.title=mdto.getTitle();
		this.release_date=mdto.getRelease_date();
	}
	
	public String getRelease_date() {
		return release_date;
	}
	public void setRelease_date(String release_date) {
		this.release_date = release_date;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MovieWithTitleAndYearDto other = (MovieWithTitleAndYearDto) obj;
		if (release_date == null) {
			if (other.release_date != null)
				return false;
		} else if (!release_date.equals(other.release_date))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}
}
