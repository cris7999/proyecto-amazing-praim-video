package com.soprasteria.smartup.dto;

import com.soprasteria.smartup.models.Actor;

public class MapperActorDtoEntity {

	public static ActorDto entityToDto(Actor ace) {
		ActorDto adto=new ActorDto();
		adto.setName(ace.getName());
		adto.setSurname(ace.getSurname());
		adto.setNumOscars(ace.getNumberOscars());
		return adto;
	}
	public static Actor dtoToEntity(ActorDto adto) {
		Actor ace=new Actor();
		ace.setName(adto.getName());
		ace.setSurname(adto.getSurname());
		ace.setNumberOscars(adto.getNumOscars());
		return ace;
	}
}