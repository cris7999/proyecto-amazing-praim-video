package com.soprasteria.smartup.repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.soprasteria.smartup.models.Act;



public interface ActRepository extends JpaRepository<Act,Integer>{

	//@Query(value="select a from Act a where a.movie=?1")
	Act findActByMovie(int idMovie);
	
	@Query(value="select * from act where id_movie=?1", nativeQuery=true)
	Collection<Act> findByMovie(int idMovie);
}
