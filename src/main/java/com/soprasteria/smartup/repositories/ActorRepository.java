package com.soprasteria.smartup.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.soprasteria.smartup.models.Actor;

public interface ActorRepository extends JpaRepository<Actor,Integer>{

}
