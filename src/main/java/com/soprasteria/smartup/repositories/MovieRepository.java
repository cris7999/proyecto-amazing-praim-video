package com.soprasteria.smartup.repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.soprasteria.smartup.models.*;
@Repository
public interface MovieRepository extends JpaRepository<Movie,Integer>{


	Collection<Movie> findByTitle(String title);
}
 