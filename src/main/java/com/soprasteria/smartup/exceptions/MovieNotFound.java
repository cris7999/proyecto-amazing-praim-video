package com.soprasteria.smartup.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason="Movie not found")
public class MovieNotFound extends RuntimeException {

	private static final long serialVersionUID = 1L;
	private String name;
	public MovieNotFound(String name) {
		super();
		this.name=name;
	}
	
	@Override
	public String getMessage() {
		return "Movie with name:"+name+" cant be found";
	}
}
