package com.soprasteria.smartup.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason="Actor not found")
public class ActorNotFound extends RuntimeException {
	private static final long serialVersionUID = 1L;

	
	public ActorNotFound() {
		super();
	}
	
	@Override
	public String getMessage() {
		return "Actor not found";
	}
}