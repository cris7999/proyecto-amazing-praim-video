package com.soprasteria.smartup.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

//naming
@ResponseStatus(value = HttpStatus.CONFLICT, reason="Other movie with the same name was inserted")
public class MovieNotInserted extends RuntimeException{

	private static final long serialVersionUID = 508843801898402728L;
	private String name;
	public MovieNotInserted(String name) {
		super();
		this.name=name;
	}
	
	@Override
	public String getMessage() {
		return "Movie with name:"+name+" cant be inserted";
	}
}
