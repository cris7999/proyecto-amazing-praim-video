package com.soprasteria.smartup.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason="Movie not found")
public class MovieNotDeleted extends RuntimeException{

	private static final long serialVersionUID = 31156036940451984L;
	private int id;
	public MovieNotDeleted(int id) {
		super();
		this.id=id;
	}
	
	@Override
	public String getMessage() {
		return "Movie with id:"+id+" cant be found";
	}
}